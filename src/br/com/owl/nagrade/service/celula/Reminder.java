package br.com.owl.nagrade.service.celula;

import java.io.Serializable;
import java.sql.ResultSet;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Innovative.admin on 11/06/2016.
 */

public class Reminder implements Serializable{

    private int id;

    private long timeRepeat;

    private String title;

    private String mensage;

    private Date date;

    private Date dateInsert;

    private long noticeBefore;

    private boolean cancelado;

    public Reminder(String title,String mensage,long timeRepeat,Date dateInsert,Date date,long noticeBefore){

        this.title = title;
        this.mensage = mensage;
        this.timeRepeat = timeRepeat;
        this.date = date;
        this.dateInsert = dateInsert;
        this.noticeBefore = noticeBefore;
    }

    public Reminder(){
    }
    
    public Reminder(ResultSet rs){
    	
    	try{
    		
    		this.id = rs.getInt("id");
    		this.timeRepeat = rs.getLong("timeRepeat");
    		this.title = rs.getString("title");
    		this.mensage = rs.getString("mensage");
    		this.date = rs.getDate("date");
    		this.dateInsert = rs.getDate("dateInsert");
    		this.noticeBefore = rs.getLong("noticeBefore");
    		this.cancelado = rs.getBoolean("cancelado");
    		
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	
    	
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public long getTimeRepeat() {
        return timeRepeat;
    }

    public void setTimeRepeat(long timeRepeat) {
        this.timeRepeat = timeRepeat;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMensage() {
        return mensage;
    }

    public void setMensage(String mensage) {
        this.mensage = mensage;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDateInsert() {
        return dateInsert;
    }

    public void setDateInsert(Date dateInsert) {
        this.dateInsert = dateInsert;
    }

    public long getNoticeBefore() {
        return noticeBefore;
    }

    public void setNoticeBefore(long noticeBefore) {
        this.noticeBefore = noticeBefore;
    }

    public boolean isCancelado() {
        return cancelado;
    }

    public void setCancelado(boolean cancelado) {
        this.cancelado = cancelado;
    }
}
