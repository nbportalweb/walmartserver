package br.com.owl.nagrade.service.celula;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Date;


import javax.ejb.Stateless;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.google.gson.Gson;
import com.mysql.jdbc.Connection;
import com.sun.xml.bind.v2.schemagen.xmlschema.List;

@Stateless
@Path("ReminderRestService")
public class ReminderRestService {
	
	@GET
    @Path("/TesteService")
	public String testeService(){
		return "ReminderRestService....servico ok";
	}
	
	@POST
	@Path("/insert")
	@Consumes(MediaType.APPLICATION_JSON)
	@Produces(MediaType.APPLICATION_JSON)
	public int insert(Reminder reminder){
		
		MysqlConnect connect = new MysqlConnect();
		
		java.sql.Connection connection = connect.connect(); 
		
		try {
			Statement stmt = connection.createStatement();
			stmt.executeQuery("insert into testewalmart.reminder (timeRepeat,title,mensage,date,dateInsert,noticeBefore,cancelado) "
					+ "values ("+reminder.getTimeRepeat()+","+reminder.getTitle()+","+reminder.getDate()+","+reminder.getDateInsert()+","+reminder.getNoticeBefore()+","+reminder.isCancelado()+")");
			return 1;
		}catch(Exception e){
			e.printStackTrace();
			return -1;
		}
		
	}
	
	@GET
    @Path("/GetAll")
	public String getAll(){
		
		MysqlConnect connect = new MysqlConnect();
		
		java.sql.Connection connection = connect.connect(); 
		
		java.util.List<Reminder> list = new ArrayList<>();
		
		try {
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT * FROM testewalmart.reminder;");
			
			while (rs.next()) {
				
				list.add(new Reminder(rs));

			}
			
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		Gson gson = new Gson();
		
		return gson.toJson(list);
	}

}
